#ifndef SEARCHWORKER_H
#define SEARCHWORKER_H

#include <QThread>
#include <QList>
#include <QtNetwork/QNetworkProxy>
#include "searchnode.h"

class SearchWorker : public QThread
{
    Q_OBJECT
public:
    SearchWorker(SearchNode *node, const QString& text);
    ~SearchWorker();
    void setProxy(QNetworkProxy::ProxyType proxyType, const QString& proxyIp, uint proxyPort);
    void setSearchNode(SearchNode *node);
private:
    void run() override;
    SearchNode *m_node;
    QString     m_text;
    QList<QString> *m_pchildUrlList;

    QNetworkProxy::ProxyType m_proxyType;
    QString m_proxyIp;
    uint    m_proxyPort;

signals:
    void nodeStatusChanged(SearchNode *node, NodeStatus status);
    void nodeErrorChanged(SearchNode *node, QString error);
    void childUrlsFound(SearchNode *node, QList<QString> *childUrlList);
    void workerFinished(SearchWorker *srchWorker, SearchNode *node);
};

#endif // SEARCHWORKER_H
