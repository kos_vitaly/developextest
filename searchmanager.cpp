#include <QDebug>
#include <QString>
#include "searchmanager.h"

//=================================================================
SearchManager::SearchManager(const QString &rootUrl, const QString &searchText, uint maxThreadCount, uint maxUrlCount)
{
    m_currentProcessLevel=0;
    m_rootNode = new SearchNode(nullptr,rootUrl,0);
    m_searchText=searchText;
    m_maxWorkers=maxThreadCount;
    m_maxProcessedNodeCount=maxUrlCount;
    m_processedNodeCount=0;
    m_urlSet.insert(rootUrl);
    m_proxyType=QNetworkProxy::NoProxy;

}
//=================================================================
SearchManager::~SearchManager()
{
    delete m_rootNode;
}
//==================================================================
SearchNode *SearchManager::getRootNode() const
{
    return m_rootNode;
}
//==================================================================
void SearchManager::setProxy(QNetworkProxy::ProxyType proxyType, const QString& proxyIp, uint proxyPort)
{
    m_proxyType=proxyType;
    m_proxyIp=proxyIp;
    m_proxyPort=proxyPort;
}
//==================================================================
void SearchManager::searchStart()
{
    m_paused=false;
    m_stopped=false;
    appendNodeToLevelList(m_rootNode, m_currentProcessLevel,m_waitingNodeList,m_maxProcessedNodeCount - m_processedNodeCount);
    processLevel();
}
//==================================================================
void SearchManager::searchPause()
{
    m_paused=true;
}
//==================================================================
void SearchManager::searchResume()
{
    m_paused=false;
    processLevel();
}
//==================================================================
void SearchManager::searchStop()
{
    m_paused=false;
    m_stopped=true;

    foreach(SearchWorker *srchWorker, m_activeWorkersList)
    {
        disconnect(srchWorker,&SearchWorker::nodeStatusChanged,this, &SearchManager::changeNodeStatus);
        disconnect(srchWorker,&SearchWorker::nodeErrorChanged, this, &SearchManager::changeNodeError);
        disconnect(srchWorker,&SearchWorker::childUrlsFound,this,&SearchManager::foundChildUrls);
        disconnect(srchWorker,&SearchWorker::workerFinished,this,&SearchManager::finishedWorker);
        connect(srchWorker,&SearchWorker::finished,srchWorker,&SearchWorker::deleteLater);
    }

    foreach(SearchWorker *srchWorker, m_waitingWorkersList)
    {
        srchWorker->deleteLater();
    }

    m_waitingNodeList.clear();
    m_processingNodeList.clear();
    m_activeWorkersList.clear();
    m_waitingWorkersList.clear();

    processLevel();
}
//==================================================================
void SearchManager::appendNodeToLevelList(SearchNode* node, uint level, QList<SearchNode *> &list,  int maxListSize)
{
    Q_ASSERT(node);
    if (node->getNodeLevel()==level)
    {
        list.push_back(node);
        emit childNodeAppended(node);
    }
    else
    {
        foreach(SearchNode *childNode, node->getChildNodes())
        {
            if (list.size() < maxListSize)
            {
                appendNodeToLevelList(childNode, level, list, m_maxProcessedNodeCount - m_processedNodeCount);
            }
            else
            {
                break;
            }
        }
    }
}
//==================================================================
void SearchManager::changeNodeStatus(SearchNode *node, NodeStatus status)
{
    Q_ASSERT(node);
    node->setNodeStatus(status);
    emit nodeStatusChanged(node,status);
}
//==================================================================
void SearchManager::changeNodeError(SearchNode *node, QString error)
{
    node->setErrorString(error);
}
//==================================================================
void SearchManager::foundChildUrls(SearchNode *node, QList<QString> *childUrlList)
{
    Q_ASSERT(node);
    foreach(QString url, *childUrlList)
    if (!m_urlSet.contains(url))
    {
        node->appendChildNode(url);
        m_urlSet.insert(url);
    }
}
//==================================================================
void SearchManager::finishedWorker(SearchWorker *srchWorker, SearchNode *node)
{
    Q_ASSERT(srchWorker);
    Q_ASSERT(node);
    m_processedNodeCount++;
    m_processingNodeList.removeOne(node);
    //SearchWorker *srchWorker=qobject_cast<SearchWorker *>(QObject::sender());
    m_activeWorkersList.removeOne(srchWorker);
    m_waitingWorkersList.push_back(srchWorker);

    if (m_processedNodeCount < m_maxProcessedNodeCount)
    {
        if (m_waitingNodeList.size()==0 && m_processingNodeList.size()==0)
        {
            m_currentProcessLevel++;
            appendNodeToLevelList(m_rootNode, m_currentProcessLevel, m_waitingNodeList,m_maxProcessedNodeCount - m_processedNodeCount);
        }
    }

    processLevel();
}
//==================================================================
void SearchManager::processLevel()
{
    if(!m_paused && !m_stopped)
    {
        while(m_processingNodeList.size() < m_maxWorkers && m_waitingNodeList.size() > 0)
        {
            SearchNode *node=m_waitingNodeList.takeFirst();
            SearchWorker *srchWorker;

            if (m_waitingWorkersList.size()>0)
            {
                srchWorker=m_waitingWorkersList.takeFirst();
                srchWorker->wait();
                srchWorker->setSearchNode(node);
            }
            else
            {
                srchWorker=new SearchWorker(node,m_searchText);
                connect(srchWorker,&SearchWorker::nodeStatusChanged,this, &SearchManager::changeNodeStatus);
                connect(srchWorker,&SearchWorker::nodeErrorChanged, this, &SearchManager::changeNodeError);
                connect(srchWorker,&SearchWorker::childUrlsFound,this,&SearchManager::foundChildUrls);
                connect(srchWorker,&SearchWorker::workerFinished,this,&SearchManager::finishedWorker);
                //deleteLater will disconnect all signals/slots after processing
                //connect(srchWorker,&SearchWorker::finished,srchWorker,&SearchWorker::deleteLater);
            }
            if (m_proxyType != QNetworkProxy::NoProxy) srchWorker->setProxy(m_proxyType, m_proxyIp, m_proxyPort);

            m_processingNodeList.push_back(node);
            m_activeWorkersList.push_back(srchWorker);

            srchWorker->start();
            //qDebug() << "PROCESSED NODES: " << m_processedNodeCount <<" PROCESSINGNODES: " << m_processingNodeList.size() << " WAITINGNODES:" << m_waitingNodeList.size();
        }
    }

    if (m_processingNodeList.size()==0 && !m_paused)
    {
         emit searchFinished();
    }

    emit statisticUpdated(m_currentProcessLevel, m_processedNodeCount,m_waitingNodeList.size()+m_processingNodeList.size(),m_processingNodeList.size());
}
//====================================================================
