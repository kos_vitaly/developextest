#ifndef SEARCHNODE_H
#define SEARCHNODE_H

#include <QObject>
#include <QList>

class QString;

enum class NodeStatus
{
    StatusWaiting=0,
    StatusDownloading,
    StatusErrorDownload,
    StatusSearchUrl,
    StatusSearchText,
    StatusFinishedTextFound,
    StatusFinishedTextNotFound
};

Q_DECLARE_METATYPE(NodeStatus)

class SearchNode
{
public:
    explicit SearchNode(SearchNode *parent, const QString &url, uint level);
    ~SearchNode();

    SearchNode*    getParent() const;
    const QString& getUrl() const;
    void           setNodeStatus(const NodeStatus status);
    NodeStatus     getNodeStatus() const;
    QString        getNodeStatusString() const;
    uint           getNodeLevel() const;
    SearchNode*    appendChildNode(const QString &childUrl);
    void           setErrorString(const QString &error);
    const QString& getErrorString() const;
    const QList<SearchNode *>& getChildNodes() const;

private:
    SearchNode* m_parent;
    QString     m_url;
    uint        m_nodeLevel;
    NodeStatus  m_nodeStatus;
    QString     m_errorString;
    QList<SearchNode *> m_childNodes;
};

#endif // SEARCHNODE_H
