#ifndef QREPLYTIMEOUT_H
#define QREPLYTIMEOUT_H

#include <QtNetwork/QNetworkReply>
#include <QTimer>

//class from here: https://codereview.stackexchange.com/questions/30031/qnetworkreply-network-reply-timeout-helper

/**
Usage:
new QReplyTimeout(yourReply, msec);

When the timeout is reached the given reply is closed if still running
*/
class QReplyTimeout : public QObject {
  Q_OBJECT
public:
  QReplyTimeout(QNetworkReply* reply, const int timeout) : QObject(reply) {
    Q_ASSERT(reply);
    if (reply) {
      QTimer::singleShot(timeout, this, SLOT(timeout()));
    }
  }

private slots:
  void timeout() {
    QNetworkReply* reply = static_cast<QNetworkReply*>(parent());
    if (reply->isRunning()) {
      reply->close();
    }
  }
};

#endif // QREPLYTIMEOUT_H
