#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QTreeWidgetItem>
#include <QRegExp>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    sm=nullptr;

    qRegisterMetaType<NodeStatus>();

    ui->treeWidget->setUniformRowHeights(true);
    ui->treeWidget->setHeaderLabels(QStringList({"URLs","Status"}));
    ui->buttonPause->setEnabled(false);
    ui->buttonStop->setEnabled(false);

    ui->comboBoxProxyType->addItem("NoProxy",QNetworkProxy::NoProxy);
    ui->comboBoxProxyType->addItem("HttpProxy",QNetworkProxy::HttpProxy);
    ui->comboBoxProxyType->addItem("Socks5Proxy",QNetworkProxy::Socks5Proxy);

    connect(ui->buttonStart,&QPushButton::clicked,this,&MainWindow::clickedButtonStart);
    connect(ui->buttonPause,&QPushButton::clicked,this,&MainWindow::clickedButtonPause);
    connect(ui->buttonStop, &QPushButton::clicked,this,&MainWindow::clickedButtonStop);
}
//=================================================================
MainWindow::~MainWindow()
{
    delete ui;
    if (sm) sm->deleteLater();
}
//=================================================================
void MainWindow::clickedButtonStart()
{
    sm = new SearchManager(ui->lineEditRootUrl->text(),ui->lineEditSearchText->text(),ui->spinBoxMaxThreadCount->value(),ui->spinBoxMaxScanUrl->value());

    connect(sm,&SearchManager::childNodeAppended,this,&MainWindow::appendChildNode);
    connect(sm,&SearchManager::nodeStatusChanged,this,&MainWindow::changeNodeStatus);
    connect(sm,&SearchManager::searchFinished,this,&MainWindow::finishSearch);
    connect(sm,&SearchManager::statisticUpdated,this,&MainWindow::updateStatistic);

    m_widgetMap.clear();
    ui->treeWidget->clear();

    if ((QNetworkProxy::ProxyType)ui->comboBoxProxyType->currentData().toInt() != QNetworkProxy::NoProxy)
    {
        QRegExp rx("^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?):(\\d+)$");

        QString ip;
        int port;
        if (rx.indexIn(ui->lineEditProxy->text()) != -1)
        {
             ip=rx.cap(1)+"."+rx.cap(2)+"."+rx.cap(3)+"."+rx.cap(4);
             port=rx.cap(5).toInt();
        }
        else
        {
            QMessageBox::information(this, "Proxy set error","Check proxy settings");
            return;
        }
        if (port < 1 || port>65535)
        {
            QMessageBox::information(this, "Proxy set error","Check proxy settings(port)");
            return;
        }

        sm->setProxy((QNetworkProxy::ProxyType)ui->comboBoxProxyType->currentData().toInt(),ip,port);
    }

    sm->searchStart();

    ui->buttonStart->setEnabled(false);
    ui->buttonPause->setEnabled(true);
    ui->buttonPause->setChecked(false);
    ui->buttonStop->setEnabled(true);
    ui->groupBoxSearchParameters->setEnabled(false);
}
//=================================================================
void MainWindow::clickedButtonStop()
{
    if (sm) sm->searchStop();
    ui->buttonPause->setEnabled(false);
    ui->buttonPause->setChecked(false);
    ui->buttonStop->setEnabled(false);
}
//=================================================================
void MainWindow::clickedButtonPause(bool checked)
{
        if (checked)
        {
            if (sm) sm->searchPause();
        }
        else
        {
            if (sm) sm->searchResume();
        }
}
//=================================================================
void MainWindow::appendChildNode(SearchNode *childNode)
{
    Q_ASSERT(childNode);
    QStringList sl;
    sl << childNode->getUrl() << childNode->getNodeStatusString();
    QTreeWidgetItem *item=new QTreeWidgetItem(sl);

    if (childNode->getParent()==nullptr) //root node
    {
        ui->treeWidget->addTopLevelItem(item);
        m_widgetMap.insert(childNode, item);
    }
    else
    {
        if (m_widgetMap.contains(childNode->getParent()))
        {
            m_widgetMap.find(childNode->getParent()).value()->addChild(item);
            m_widgetMap.insert(childNode, item);
            ui->treeWidget->expandAll();
        }
    }
}
//=================================================================
void MainWindow::changeNodeStatus(SearchNode *node, NodeStatus status)
{
    Q_ASSERT(node);
    QTreeWidgetItem *item=m_widgetMap.find(node).value();

    if (status!=NodeStatus::StatusErrorDownload)
    {
        item->setText(1,node->getNodeStatusString());
    }
    else
    {
        item->setText(1,node->getNodeStatusString() + ": " + node->getErrorString());
    }

    switch (status)
    {
        case NodeStatus::StatusWaiting:
             break;
        case NodeStatus::StatusDownloading :
        case NodeStatus::StatusSearchUrl :
        case NodeStatus::StatusSearchText :
            item->setBackgroundColor(1,Qt::yellow);
            break;
        case NodeStatus::StatusErrorDownload :
            item->setBackgroundColor(1,Qt::gray);
            break;
        case NodeStatus::StatusFinishedTextFound :
            item->setBackgroundColor(1,Qt::green);
            break;
        case NodeStatus::StatusFinishedTextNotFound :
            item->setBackgroundColor(1,Qt::red);
            break;
    }
}
//=================================================================
void MainWindow::finishSearch()
{
    if(sm) sm->deleteLater();
    sm=nullptr;

    ui->buttonStart->setEnabled(true);
    ui->buttonPause->setEnabled(false);
    ui->buttonPause->setChecked(false);
    ui->buttonStop->setEnabled(false);
    ui->groupBoxSearchParameters->setEnabled(true);
}
//=================================================================
void MainWindow::updateStatistic(uint scanningLevel, uint scannedUrls, uint urlsLeft, uint workingThreads)
{
    ui->lineEditScanningLevel->setText(QString::number(scanningLevel));
    ui->lineEditThreadsCount->setText(QString::number(workingThreads));
    ui->lineEditScannedUrls->setText(QString::number(scannedUrls));
    ui->lineEditUrlsLeft->setText(QString::number(urlsLeft));
    ui->progressBar->setValue(scannedUrls*100.0 / ui->spinBoxMaxScanUrl->value());
}
//=================================================================
