#include <QDebug>
#include <QString>
#include "searchnode.h"

//=====================================================
SearchNode::SearchNode(SearchNode *parent, const QString &url, uint level):m_parent(parent),m_url(url),m_nodeLevel(level)
{
    m_nodeStatus=NodeStatus::StatusWaiting;
}
//=====================================================
SearchNode::~SearchNode()
{
    foreach(SearchNode *pChildNode, m_childNodes)
    {
        delete pChildNode;
    }
}
//=====================================================
SearchNode* SearchNode::getParent() const
{
    return m_parent;
}
//=====================================================
const QString& SearchNode::getUrl() const
{
    return m_url;
}
//=====================================================
void SearchNode::setNodeStatus(NodeStatus status)
{
    m_nodeStatus=status;
}
//=====================================================
NodeStatus SearchNode::getNodeStatus() const
{
    return m_nodeStatus;
}
//=====================================================
QString SearchNode::getNodeStatusString() const
{
    switch (m_nodeStatus)
    {
        case NodeStatus::StatusWaiting:
            return "";
        case NodeStatus::StatusDownloading:
            return "downloading";
        case NodeStatus::StatusErrorDownload:
            return "errorDownload";
        case NodeStatus::StatusSearchUrl:
            return "searchUrl";
        case NodeStatus::StatusSearchText:
            return "searchText";
        case NodeStatus::StatusFinishedTextFound:
            return "textFound";
        case NodeStatus::StatusFinishedTextNotFound:
            return "textNotFound";
        default:
            return "statusUndefined";
    }
}
//=====================================================
uint SearchNode::getNodeLevel() const
{
    return m_nodeLevel;
}
//=====================================================
SearchNode* SearchNode::appendChildNode(const QString &childUrl)
{
    SearchNode *childNode=new SearchNode(this, childUrl,getNodeLevel()+1);
    m_childNodes.push_back(childNode);
    return childNode;
}
//=====================================================
void SearchNode::setErrorString(const QString &error)
{
    m_errorString=error;
}
//=====================================================
const QString &SearchNode::getErrorString() const
{
    return m_errorString;
}
//=====================================================
const QList<SearchNode *>& SearchNode::getChildNodes() const
{
    return m_childNodes;
}
//=====================================================
