#ifndef SEARCHMANAGER_H
#define SEARCHMANAGER_H

#include <QObject>
#include <QList>
#include <QSet>
#include "searchworker.h"

class QString;

class SearchManager : public QObject
{
    Q_OBJECT
public:
    SearchManager(const QString& rootUrl, const QString& searchText, uint maxThreadCount, uint maxUrlCount);
    ~SearchManager();
    SearchNode* getRootNode() const;
    void setProxy(QNetworkProxy::ProxyType proxyType, const QString& proxyIp, uint proxyPort);
    void searchStart();
    void searchPause();
    void searchResume();
    void searchStop();

private:
    SearchNode *m_rootNode;
    QString m_searchText;
    QSet<QString> m_urlSet;
    QList<SearchNode *> m_waitingNodeList;
    QList<SearchNode *> m_processingNodeList;
    QList<SearchWorker *> m_activeWorkersList;
    QList<SearchWorker *> m_waitingWorkersList;

    int  m_maxWorkers;
    uint m_processedNodeCount;
    uint m_maxProcessedNodeCount;
    uint m_currentProcessLevel;
    bool m_paused;
    bool m_stopped;

    QNetworkProxy::ProxyType m_proxyType;
    QString m_proxyIp;
    uint    m_proxyPort;

    void appendNodeToLevelList(SearchNode* node, uint level, QList<SearchNode *> &list, int maxListSize);
    void processLevel();

public slots:
    void changeNodeStatus(SearchNode *node, NodeStatus status);
    void changeNodeError(SearchNode *node, QString error);
    void foundChildUrls(SearchNode *node, QList<QString> *childUrlList);
    void finishedWorker(SearchWorker *srchWorker, SearchNode *node);

signals:
    void childNodeAppended(SearchNode *childNode);
    void nodeStatusChanged(SearchNode *node, NodeStatus status);
    void searchFinished();
    void statisticUpdated(uint scanningLevel, uint scannedUrls, uint urlsLeft, uint workingThreads);
};

#endif // SEARCHMANAGER_H
