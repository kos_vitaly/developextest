#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QEventLoop>
#include <QTextCodec>
#include "searchworker.h"
#include "qreplytimeout.h"
//============================================================
SearchWorker::SearchWorker(SearchNode *node, const QString &text)
{
    Q_ASSERT(node);
    m_node=node;
    m_text=text;
    m_pchildUrlList = new QList<QString>;
    m_proxyType=QNetworkProxy::NoProxy;
}
//============================================================
SearchWorker::~SearchWorker()
{
    delete m_pchildUrlList;
}
//============================================================
void SearchWorker::setProxy(QNetworkProxy::ProxyType proxyType, const QString& proxyIp, uint proxyPort)
{
    m_proxyType=proxyType;
    m_proxyIp=proxyIp;
    m_proxyPort=proxyPort;
}
//============================================================
void SearchWorker::setSearchNode(SearchNode *node)
{
    Q_ASSERT(node);
    m_node=node;
    m_pchildUrlList->clear();
}
//============================================================
void SearchWorker::run()
{
    emit nodeStatusChanged(m_node,NodeStatus::StatusDownloading);

    QEventLoop eventLoop;
    QNetworkAccessManager networkAccessManager;
    QNetworkProxy proxy;

    if (m_proxyType != QNetworkProxy::NoProxy)
    {
        proxy.setType(m_proxyType);
        proxy.setHostName(m_proxyIp);
        proxy.setPort(m_proxyPort);
        networkAccessManager.setProxy(proxy);
    }

    QNetworkRequest networkRequest(QUrl(m_node->getUrl()));
    QNetworkReply *networkReply = networkAccessManager.get(networkRequest);
    connect(networkReply, &QNetworkReply::finished, &eventLoop, &QEventLoop::quit);
    new QReplyTimeout(networkReply,30000);
    eventLoop.exec();

    if (networkReply->error() != QNetworkReply::NoError)
    {
        emit nodeErrorChanged(m_node, networkReply->errorString());
        emit nodeStatusChanged(m_node,NodeStatus::StatusErrorDownload);
        emit workerFinished(this, m_node);
        return;
    }

    //QString data=networkReply->readAll();
    QByteArray buffer = networkReply->readAll();
    QString data=QTextCodec::codecForHtml(buffer)->toUnicode(buffer);

    //поиск дочерних Url
    emit nodeStatusChanged(m_node,NodeStatus::StatusSearchUrl);

    QRegExp rx("https?:\\/\\/[\\d\\/a-zA-Z\\+\\-&@#%=~_|\\$\\?!:,\\.]+"); // create the regular expression

    int posStartUrl=0;
    int posLengthUrl=0;
    while ( (posStartUrl = rx.indexIn(data,posStartUrl+posLengthUrl)) != -1 ) // while there is a matching substring
    {
         posLengthUrl=rx.cap(0).length();
         m_pchildUrlList->push_back(rx.cap(0));
    }

    emit childUrlsFound(m_node, m_pchildUrlList);

    //поиск подстроки
    emit nodeStatusChanged(m_node,NodeStatus::StatusSearchText);

    if (data.contains(m_text,Qt::CaseSensitive))
    {
        emit nodeStatusChanged(m_node,NodeStatus::StatusFinishedTextFound);
    }
    else
    {
        emit nodeStatusChanged(m_node,NodeStatus::StatusFinishedTextNotFound);
    }

    emit workerFinished(this, m_node);
}
//============================================================
