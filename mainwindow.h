#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QHash>
#include "searchmanager.h"


class QTreeWidgetItem;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    SearchManager *sm;
    QHash<SearchNode*,QTreeWidgetItem*> m_widgetMap;


private slots:
    void clickedButtonStart();
    void clickedButtonStop();
    void clickedButtonPause(bool checked);

    void appendChildNode(SearchNode *childNode);
    void changeNodeStatus(SearchNode *node, NodeStatus status);
    void finishSearch();
    void updateStatistic(uint scanningLevel, uint scannedUrls, uint urlsLeft, uint workingThreads);
};

#endif // MAINWINDOW_H
