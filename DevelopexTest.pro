#-------------------------------------------------
#
# Project created by QtCreator 2018-12-10T13:29:54
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = DevelopexTest
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    searchmanager.cpp \
    searchworker.cpp \
    searchnode.cpp

HEADERS  += mainwindow.h \
    searchmanager.h \
    searchworker.h \
    searchnode.h \
    qreplytimeout.h

FORMS    += mainwindow.ui

#LIBS += -lws32_2

RESOURCES += \
    res.qrc

OTHER_FILES += \
    CommentsToProgram.txt
